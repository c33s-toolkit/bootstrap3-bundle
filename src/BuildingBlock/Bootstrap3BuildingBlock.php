<?php

namespace C33s\Toolkit\Bootstrap3Bundle\BuildingBlock;

use C33s\ConstructionKitBundle\BuildingBlock\SimpleBuildingBlock;

class Bootstrap3BuildingBlock extends SimpleBuildingBlock
{
    /**
     * Return true if this block should be installed automatically as soon as it is registered (e.g. using composer).
     * This is the only public method that should not rely on a previously injected Kernel.
     *
     * @return bool
     */
    public function isAutoInstall()
    {
        return true;
    }

    /**
     * Get the fully namespaced classes of all bundles that should be enabled to use this BuildingBlock.
     * These will be used in AppKernel.php.
     *
     * @return array
     */
    public function getBundleClasses()
    {
        return array(
            'C33s\Toolkit\Bootstrap3Bundle\C33sToolkitBootstrap3Bundle',
        );
    }

    /**
     * Return all assets that can be added automatically by this BuildingBlock. Return array grouped by asset type.
     * e.g.
     *
     * return array(
     *     "webpage_js_top" => array('path/to/jquery.js'),
     *     "webpage_js"     => array('path/to/lib1.js', 'path/to/lib2.js'),
     *     "admin_css"      => array('path/to/some.css'),
     * );
     *
     * Asset paths must be provided in a way that allows them to be loaded using Assetic.
     * The usage of this feature highly depends on your specific project structure.
     *
     * @return array
     */
    public function getAssets()
    {
        return array(
            'c33s_app_stylesheets' => array(
                '@AppBundle/Resources/private/less/app/app.less',
                'media/components/vegas/dist/vegas.min.css',
            ),
            'c33s_app_top_javascripts' => array(
                'media/components/jquery/jquery.min.js',
            ),
            'c33s_app_javascripts' => array(
                'media/components/form/jquery.form.js',
                'media/components/jquery-cookie/jquery.cookie.js',
                'media/components/jquery-ui/jquery-ui.js',
                'media/components/vegas/dist/vegas.js',
                '../vendor/twbs/bootstrap/js/affix.js',
                '../vendor/twbs/bootstrap/js/alert.js',
                '../vendor/twbs/bootstrap/js/button.js',
                '../vendor/twbs/bootstrap/js/carousel.js',
                '../vendor/twbs/bootstrap/js/collapse.js',
                '../vendor/twbs/bootstrap/js/dropdown.js',
                '../vendor/twbs/bootstrap/js/modal.js',
                '../vendor/twbs/bootstrap/js/tooltip.js',
                '../vendor/twbs/bootstrap/js/popover.js',
                '../vendor/twbs/bootstrap/js/scrollspy.js',
                '../vendor/twbs/bootstrap/js/tab.js',
                '../vendor/twbs/bootstrap/js/transition.js',
                '@AppBundle/Resources/private/js/app.js',
            ),
        );
    }
}
