# c33s-toolkit/bootstrap3-bundle

Twitter Bootstrap 3 integration into [c33s-toolkit/layout-bundle](https://packagist.org/packages/c33s-toolkit/layout-bundle) layouts.

## Installation

Follow documentation for [c33s-toolkit/layout-bundle](https://gitlab.com/c33s-toolkit/layout-bundle/)

`composer require c33s-toolkit/bootstrap3-bundle`

Add bundle to your AppKernel.php:

```php
<?php

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // ...
            new C33s\Toolkit\Bootstrap3Bundle\C33sToolkitBootstrap3Bundle(),
```
